import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css']
})
export class FeatureComponent implements OnInit {

  @Input('feature') feature;
  @Output() onFeatureUpdated = new EventEmitter<string>();
  constructor() { }
  onTitleKey(value: string) {
    this.feature.title = value;
    this.onFeatureUpdated.emit(this.feature);
  }
  onBodyKey(value: string) {
    this.feature.body = value;
    this.onFeatureUpdated.emit(this.feature);
  }

  ngOnInit() {
  }

}
