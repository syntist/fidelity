import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {TreeModule} from 'ng2-tree'

import { AppComponent } from './app.component';
import { FeatureComponent } from './feature/feature.component';
import { BackgroundComponent } from './background/background.component';
import { ScenarioComponent } from './scenario/scenario.component';
import { ScenarioListComponent } from './scenario-list/scenario-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    FeatureComponent,
    BackgroundComponent,
    ScenarioComponent,
    ScenarioListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    TreeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
