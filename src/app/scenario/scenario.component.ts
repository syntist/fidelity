import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-scenario',
  templateUrl: './scenario.component.html',
  styleUrls: ['./scenario.component.css']
})
export class ScenarioComponent implements OnInit {

  buttonTitle="New Scenario";
  @Input() currentScenario;
  @Output() onScenarioAdded=new EventEmitter<any>();
  constructor() {
   }

  onScenarioAdd(){
 	if(this.currentScenario.scenario.title==""
 		|| this.currentScenario.scenario.body=="") {
 		alert("Please Enter Scenario Title & Body");
 		return;
 	}
  	this.onScenarioAdded.emit(this.currentScenario.scenario);
  }
  ngOnInit() {
  	
  	}


  }


