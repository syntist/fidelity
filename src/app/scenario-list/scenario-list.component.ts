import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-scenario-list',
  templateUrl: './scenario-list.component.html',
  styleUrls: ['./scenario-list.component.css']
})
export class ScenarioListComponent implements OnInit {

   @Input('scenarios') scenarios;
   @Output('onScenarioSelected') onScenarioSelected =new EventEmitter<any>();
  constructor() { }

  onSelect(scenario):void{
  	this.onScenarioSelected.emit(scenario);
  }

  ngOnInit() {
  }

}
