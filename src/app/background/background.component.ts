import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.css']
})
export class BackgroundComponent implements OnInit {

  @Input('background') background;
  @Output() onBackgroundUpdated = new EventEmitter<string>();
  constructor() { }
  onTitleKey(value: string) {
    this.background.title = value;
    this.onBackgroundUpdated.emit(this.background);
  }
  onBodyKey(value: string) {
    this.background.body = value;
    this.onBackgroundUpdated.emit(this.background);
  }

  ngOnInit() {
  }

}
