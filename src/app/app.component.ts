import { Component , ViewChild} from '@angular/core';
import { TreeModel } from 'ng2-tree';
import { saveAs } from 'file-saver';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'app';
  feature : iModel;
  background: iModel;
  tags = [];
  currentScenarioWrapper;
  scenarioList=[];
  metaTagString;
  jarPath="";
  featureDirectory="";
  
  constructor(){
  	this.feature={title:'', body:'' };
  	this.background={title:'', body:'' };
  	this.currentScenarioWrapper={scenario:{title:"", body:""}};
  }
	onRun(event)
	{
		debugger;
			if(this.jarPath=="") {
				alert("Please Enter Jar Path");
				return;
			}
			if(this.featureDirectory=="") {
				alert("Please Enter Feature Directory");
				return;
			}

		    	var command = 'java -cp target/classes/:target/*:target/{{cucumberPath}} com.cucumbumbler.app.CucumbumblerMain "$@" -b -f {{featureDirectory}}';
		    	command=command.replace("{{cucumberPath}}", this.jarPath);
		    	command=command.replace("{{featureDirectory}}", this.featureDirectory);
		    	// command + ='com.cucumbumbler.app.CucumbumblerMain "$@"  -b -f ';
		    	var blob = new Blob([command], {type: "text/plain;charset=utf-8"});
				
		    	if(this.getOS()=="Windows")
				saveAs(blob, "cucumber.bat");
				else
				saveAs(blob, "cucumber.sh");
	}
	onFileLoad(event) {
    let input = event.target;
	for (var index = 0; index < input.files.length; index++) {
		let reader = new FileReader();
		reader.onload = () => {
			this.onNew();
			// this 'text' is the content of the file
			const regex = /@(.*)|Feature: (.*)|Background: ((.*)\n[\s\S]*?\n\n)|Scenario: ((.*)\n[\s\S]*?\n\n)/g;
			const str = reader.result;
			let m;

			while ((m = regex.exec(str)) !== null) {
				// This is necessary to avoid infinite loops with zero-width matches
				if (m.index === regex.lastIndex) {
					regex.lastIndex++;
				}
				// The result can be accessed through the `m`-variable.
				var latestScenario = { title: '', body: '' };
				m.forEach((match, groupIndex) => {
					if (match != null && match.length > 0) {
						switch (groupIndex) {
							case 1: this.tags.push(match); break;
							case 2: this.feature.title = match; break;
							case 3: this.background.body = match.replace(/.*\n/, "").replace(/\t/g, "").replace(/\n*$/, ""); break;
							case 4: this.background.title = match; break;
							case 5: latestScenario.body = match.replace(/.*\n/, "").replace(/\t/g, "").replace(/\n*$/, ""); break;
							case 6: latestScenario.title = match; break;
							default: break;
						}
						if (latestScenario.title != "" && latestScenario.body != "") {
							this.scenarioList.push(latestScenario);
							latestScenario = { title: '', body: '' };
						}
					}

				});

			}
			this.metaTagString = this.tags.join();
		}
		reader.readAsText(input.files[index]);
	}
 }
    onMetaSearch(value):void {
    	this.tags = this.metaTagString.split(',');

    }
    onFeatureUpdated(feature):void{
    	this.feature=feature;
    }
    onBackgroundUpdated(background):void{
    	this.background=background;
    }
    onScenarioAdded(scenario):void{
    	if(this.scenarioList.indexOf(scenario)==-1){
    		this.scenarioList.push(scenario);
    	}
    	this.currentScenarioWrapper.scenario={title:"", body:""};
    }
    onScenarioSelected(scenario):void{
    	var toBeRemoved;
		this.scenarioList.forEach((item,index)=>{
			if(item.title==scenario.title && item.body==scenario.body)
			{
				toBeRemoved=index;
			}
		});  
		//this.scenarioList.splice(toBeRemoved,1);  	 
    	this.currentScenarioWrapper.scenario=scenario;
    }
    createFile():string{
    	var featureText="";
    	var tab="\t";
    	var newLine="\n";
    	this.tags.forEach(item=>{
    		featureText+="@"+item+"\n";
    	});
    	featureText+=newLine;
    	featureText+="Feature: "+this.feature.title;
    	featureText+=newLine+newLine;
    	featureText+=tab;
    	featureText+="Background: "+this.background.title;
    	featureText+=newLine+tab+tab;
    	featureText+=this.background.body;
    	featureText+=newLine+newLine+tab;
    	this.scenarioList.forEach(item=>{
    		featureText+="Scenario: "+item.title;
    		featureText+=newLine;
    		featureText+=tab+tab+item.body.replace(/\n/g, '\n\t\t');
    		featureText+=newLine+newLine+tab;
    	});
    	return featureText;
    }
    onSave(){
    	
    	var blob = new Blob([this.createFile()], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "cucumber.feature");
    }
    getOS() {
      var userAgent = window.navigator.userAgent,
          platform = window.navigator.platform,
          macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
          windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
          iosPlatforms = ['iPhone', 'iPad', 'iPod'],
          os = null;

      if (macosPlatforms.indexOf(platform) !== -1) {
        os = 'Mac OS';
      } else if (iosPlatforms.indexOf(platform) !== -1) {
        os = 'iOS';
      } else if (windowsPlatforms.indexOf(platform) !== -1) {
        os = 'Windows';
      } else if (/Android/.test(userAgent)) {
        os = 'Android';
      } else if (!os && /Linux/.test(platform)) {
        os = 'Linux';
      }

      return os;
    }
    onNew()
    {
    	this.tags.splice(0);
    	this.metaTagString='';
	    this.feature={title:'', body:'' };
	  	this.background.title="";
	  	this.background.body="";
	  	this.currentScenarioWrapper={scenario:{title:"", body:""}};
	  	this.scenarioList.splice(0);
    }

     @ViewChild('treeComponent') treeComponent;
      @ViewChild('metaTag') metaTag;


	  ngAfterViewInit(): void {
	    this.treeComponent.getControllerByNodeId(1).collapse();
	  }
    public ffs2: TreeModel=
      {value:"CucumberSolution",
      id:1,
      settings: {
       leftMenu: false,
       rightMenu: false,
      cssClasses: {
        expanded: 'fa fa-caret-down',
        collapsed: 'fa fa-caret-right',
        empty: 'fa fa-caret-right disabled',
        leaf: 'fa'
      },
      templates: {
        node: '<i class="fa fa-folder-o"></i>',
        leaf: '<i class="fa fa-file-o"></i>'
      }},
      children:[
      {value:"App_Code",
      children:[{value:"BundleConfig.c"},
                {value:"CucumberNS.c"},
                {value:"IdentityModels.c"},
                {value:"RouteConfig.c"},
                {value:"Startup.Auth.c"},
                {value:"Startup.cs"}
               ]
      },
      {value:"Content",
      children:[
                  {value:"bootstrap.css"},
                  {value:"bootstrap.min.css"},
                  {value:"Site.css"},
              ]
      },
      {value:"Scripts",
      children:[,
                  {value:"bootstrap.js"},
                  {value:"bootstrap.min.js"},
                  {value:"jquery-1.10.2.intellisense.js"},
                  {value:"jquery-1.10.2.js"},
                  {value:"jquery-1.10.2.min.js"},
                  {value:"jquery-1.10.2.min.map"},
                  {value:"modernizr-2.6.2.js"},
                  {value:"respond.js"},
                  {value:"respond.min.js"},
                  {value:"_references.js"},
              ]
      },
      {value:"UploadedFiles",
      children:[
                {value:"abc (1).feature"},
                {value:"abc (2).feature"},
                {value:"abc.feature"},
                {value:"abcd.feature"},
              ]
      },
      {value:"aaa.feature"},
      {value:"About.aspx"},
      {value:"About.aspx.cs"},
      {value:"Bundle.config"},
      {value:"Default.aspx"},
      {value:"Default.aspx.cs"},
      {value:"favicon.ico"},
      {value:"favicon1.ico"},
      {value:"Global.asax"},
      {value:"packages.config"},
      {value:"Site.master"},
      {value:"Site.master.cs"},
      {value:"Site.Mobile.master"},
      {value:"Site.Mobile.master.cs"},
      {value:"ViewSwitcher.ascx"},
      {value:"ViewSwitcher.ascx.cs"},
      {value:"Web.config"}
      ]
      }
}
export interface iModel{
	title,
	body
}